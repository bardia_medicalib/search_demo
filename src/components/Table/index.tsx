import * as React from 'react';
import clsx from 'clsx';
import { withStyles, WithStyles } from '@mui/styles';
import { Theme, createTheme } from '@mui/material/styles';
import TableCell from '@mui/material/TableCell';
import Paper from '@mui/material/Paper';
import {
    AutoSizer,
    Column,
    Table,
    TableCellRenderer,
    TableHeaderProps,
    InfiniteLoader
} from 'react-virtualized';

const styles = (theme: Theme) =>
    ({
        flexContainer: {
            display: 'flex',
            alignItems: 'center',
            boxSizing: 'border-box',
        },
        table: {
            '& .ReactVirtualized__Table__headerRow': {
                ...(theme.direction === 'rtl' && {
                    paddingLeft: '0 !important',
                }),
                ...(theme.direction !== 'rtl' && {
                    paddingRight: undefined,
                }),
            },
        },
        tableRow: {
            cursor: 'pointer',
        },
        tableRowHover: {
            '&:hover': {
                backgroundColor: theme.palette.grey[200],
            },
        },
        tableCell: {
            flex: 1,
        },
        noClick: {
            cursor: 'initial',
        },
    } as const);

interface ColumnData {
    dataKey: string;
    label: string;
    numeric?: boolean;
    width: number;
}

interface Row {
    index: number;
}

interface MuiVirtualizedTableProps extends WithStyles<typeof styles> {
    columns: readonly ColumnData[];
    headerHeight?: number;
    onRowClick?: () => void;
    rowCount: number;
    rowGetter: (row: Row) => Data;
    rowHeight?: number;
    rowsData: any;
    loadMoreRows: () => void;
}

class MuiVirtualizedTable extends React.PureComponent<MuiVirtualizedTableProps> {
    static defaultProps = {
        headerHeight: 48,
        rowHeight: 50,
    };

    getRowClassName = ({ index }: Row) => {
        const { classes, onRowClick } = this.props;

        return clsx(classes.tableRow, classes.flexContainer, {
            [classes.tableRowHover]: index !== -1 && onRowClick != null,
        });
    };

    cellRenderer: TableCellRenderer = ({ cellData, columnIndex }) => {
        const { columns, classes, rowHeight, onRowClick } = this.props;
        return (
            <TableCell
                component="div"
                className={clsx(classes.tableCell, classes.flexContainer, {
                    [classes.noClick]: onRowClick == null,
                })}
                variant="body"
                title={cellData}
                style={{ height: rowHeight,
                    textOverflow: "ellipsis",
                    whiteSpace: "nowrap",
                    overflow: "hidden",
                    display: "inline-block" }}
                align={
                    (columnIndex != null && columns[columnIndex].numeric) || false
                        ? 'right'
                        : 'left'
                }
            >
                {cellData}
            </TableCell>
        );
    };

    headerRenderer = ({
                          label,
                          columnIndex,
                      }: TableHeaderProps & { columnIndex: number }) => {
        const { headerHeight, columns, classes } = this.props;

        return (
            <TableCell
                component="div"
                className={clsx(classes.tableCell, classes.flexContainer, classes.noClick)}
                variant="head"
                style={{ height: headerHeight }}
                align={columns[columnIndex].numeric || false ? 'right' : 'left'}
            >
                <span>{label}</span>
            </TableCell>
        );
    };

    render() {
        const { classes, columns, rowHeight, headerHeight, ...tableProps } = this.props;
        return (
            <InfiniteLoader
                isRowLoaded={({ index}) => !!this.props.rowsData[index]}
                loadMoreRows={this.props.loadMoreRows}
                rowCount={1000000}
            >
                {({onRowsRendered, registerChild}) => (
                    <AutoSizer>
                        {({ height, width }) => (
                            <Table
                                ref={registerChild}
                                onRowsRendered={onRowsRendered}
                                height={height}
                                width={width}
                                rowHeight={rowHeight!}
                                gridStyle={{
                                    direction: 'inherit',
                                }}
                                headerHeight={headerHeight!}
                                className={classes.table}
                                {...tableProps}
                                rowClassName={this.getRowClassName}
                            >
                                {columns.map(({ dataKey, ...other }, index) => {
                                    return (
                                        <Column
                                            key={dataKey}
                                            headerRenderer={(headerProps) =>
                                                this.headerRenderer({
                                                    ...headerProps,
                                                    columnIndex: index,
                                                })
                                            }
                                            className={classes.flexContainer}
                                            cellRenderer={this.cellRenderer}
                                            dataKey={dataKey}
                                            {...other}
                                        />
                                    );
                                })}
                            </Table>
                        )}
                    </AutoSizer>
                )}
            </InfiniteLoader>
        );
    }
}

const defaultTheme = createTheme();
const VirtualizedTable = withStyles(styles, { defaultTheme })(MuiVirtualizedTable);

interface tableProps {
    columns: {
        width: number;
        label: string;
        dataKey: string;
        numeric?: boolean;
    }[];
    rowsData: any;
    loadMoreRows: () => void;
}

interface Data {
    identifier: string,
    title: string,
    downloads: number,
    creator: string,
    mediatype: string
}

const ReactVirtualizedTable: React.FC<tableProps> = (props) => {

    const rows: Data[] = props.rowsData;

    return (
        <Paper style={{ height: 400, width: '100%' }}>
            <VirtualizedTable
                rowCount={rows.length}
                rowGetter={({ index }) => rows[index]}
                columns={props.columns}
                rowsData={props.rowsData}
                loadMoreRows={props.loadMoreRows}
            />
        </Paper>
    );
}

export default ReactVirtualizedTable;