import React from "react";
import TextField from '@mui/material/TextField';

interface InputProps {
    id: string;
    label: string;
    defaultValue: string;
    disabled: boolean;
    type: string;
    fullWidth: boolean;
    InputProps?: object;
}

const Input: React.FC<InputProps> = ({ ...args }) => {
    return (
        <TextField
            {...args}
        />
    )
}

export default Input;