import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import Btn from '../components/Button';

export default {
  title: 'Example/Button',
  component: Btn,
} as ComponentMeta<typeof Btn>;

const Template: ComponentStory<typeof Btn> = (args) => <Btn {...args} />;

export const Contained = Template.bind({});
Contained.args = {...{
  variant: 'contained',
  size: 'medium',
  children: 'BUTTON'
}};

export const Outlined = Template.bind({});
Outlined.args = {...{
  variant: 'outlined',
  size: 'medium',
  children: 'BUTTON'
}};

export const Small = Template.bind({});
Small.args = {...{
  variant: 'contained',
  size: 'small',
  children: 'small'
}};

export const Medium = Template.bind({});
Medium.args = {...{
  variant: 'contained',
  size: 'medium',
  children: 'medium'
}};

export const Large = Template.bind({});
Large.args = {...{
  variant: 'contained',
  size: 'large',
  children: 'large'
}};
