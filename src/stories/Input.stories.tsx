import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import Input from "../components/Input";

export default {
  title: 'Example/Input',
  component: Input,
  argTypes: {
    onChange: {
      action: 'onChange'
    }
  },
} as ComponentMeta<typeof Input>;

const Template: ComponentStory<typeof Input> = (args) => <Input {...args} />;

export const Default = Template.bind({});
Default.args = {
  id: 'test-outlined',
  label: 'Default',
  defaultValue: 'Test'
};

export const Disabled = Template.bind({});
Disabled.args = {
  disabled: true,
  id: 'outlined-disabled',
  label: 'Default',
  defaultValue: 'Test'
};

export const SearchField = Template.bind({});
SearchField.args = {
  id: 'outlined-search',
  label: 'Search field',
  type: 'search'
};
