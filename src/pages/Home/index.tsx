import React, {useContext, useEffect, useState} from "react";
import SearchTable from "../Search/SearchTable";
import appContext from "../../context/appContext";
import { Grid, Typography } from "@mui/material";
import Button from "../../components/Button";

const Home:React.FC = () => {
    const [favoritesData, setFavoritesData] = useState([]);

    const context = useContext(appContext);

    const receivedFavoritesFromContext = context.favorites;
    useEffect(() => {
        const mappedData = receivedFavoritesFromContext.map(item => (
            {...item, favorite: item.favorite = <Button
                {...{size: 'small',
                    onClick: () => context.removeItemFromFavorites(item.identifier)
                }}
            >Remove
        </Button>}));
        console.log(mappedData)
        setFavoritesData(mappedData);
    }, [receivedFavoritesFromContext]);

    return (
        <Grid container
              direction="row"
              spacing={2}
              justifyContent={"center"}
        >
            {
                context.favorites && context.favorites.length ?
                    <SearchTable
                        searchData={favoritesData}
                    /> :
                    <Typography textAlign={"center"} mt={20}>
                        Your favorites list is empty.
                    </Typography>
            }

        </Grid>
    )
}

export default Home;