import React from "react";
import Header from "../../../components/Header";
import {Container, Box, Grid} from "@mui/material";

const DefaultLayout: React.FC = ({ children }) => {

    const menuItems = [
        {
            id: 1,
            title: "Home",
            href: "/"
        },
        {
            id: 2,
            title: "Search",
            href: "/search"
        }
    ];

    return (
        <Container maxWidth={'xl'}>
            <Header
                logoTitle={"Search Demo"}
                menuItems={menuItems}
            />
            <Box mt={11}>
                { children }
            </Box>
        </Container>
    )
}

export default DefaultLayout;