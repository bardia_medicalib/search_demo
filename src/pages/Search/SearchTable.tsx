import React from "react";
import { Box, Grid } from "@mui/material";
import ReactVirtualizedTable from "../../components/Table";
import CircularProgress from "@mui/material/CircularProgress";

interface searchTableProps {
    status?: string;
    searchData: any;
    handleDoSearch?(flag: boolean): void;
    handleFocusOnInput?: () => void;
}

const SearchTable:React.FC<searchTableProps> = (props) => {

    const columns = [
        {
            width: 250,
            label: 'Identifier',
            dataKey: 'identifier',
        },
        {
            width: 200,
            label: 'Title',
            dataKey: 'title',
        },
        {
            width: 200,
            label: 'Downloads',
            dataKey: 'downloads',
            numeric: true,
        },
        {
            width: 200,
            label: 'Creator',
            dataKey: 'creator',
        },
        {
            width: 150,
            label: 'Media type',
            dataKey: 'mediatype',
        },
        {
            width: 180,
            label: '',
            dataKey: 'favorite',
        }
    ];

    return (
        <Grid width={"100%"} display={"flex"} justifyContent={"center"}>
            <Box height={400} width={900} mt={8}>
                {
                    <>
                        <ReactVirtualizedTable
                            columns={columns}
                            rowsData={props.searchData}
                            loadMoreRows={() => props.handleDoSearch && props.handleDoSearch(false)}
                        />
                        {
                            props.status === "fetching" &&
                            <Box textAlign={"center"} mt={1}>
                              <CircularProgress color="inherit" />
                            </Box>
                        }
                    </>
                }
            </Box>
        </Grid>
    )
}

export default SearchTable;