import React, { useReducer } from "react";
import AppContext from "./appContext";
import { appReducer } from "./reducers";
import { ADD_ITEM_TO_FAVORITES, REMOVE_ITEM_FROM_FAVORITES } from "./reducers";

const GlobalState:React.FC = (props) => {
    const [appState, dispatch] = useReducer(appReducer, { favorites: [] });

    const addItemToFavorites = (item) => {
        dispatch({ type: ADD_ITEM_TO_FAVORITES, payload: item });
    }

    const removeItemFromFavorites = (itemId) => {
        dispatch({ type: REMOVE_ITEM_FROM_FAVORITES, itemId });
    }

    return (
        <AppContext.Provider value={{
            favorites: appState && appState.favorites,
            addItemToFavorites,
            removeItemFromFavorites
        }}>
            { props.children }
        </AppContext.Provider>
    )
}

export default GlobalState;